# Weltrend MCU control daemon and console tool

This is a control facility for Weltrend MCUs that could be found in
Western Digital's My Cloud devices (like EX2/Ultra) suited for usage
in OpenWrt-based environments.