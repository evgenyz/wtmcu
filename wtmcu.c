#include <errno.h>
#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <syslog.h>

#include "wtmcu.h"


struct wtmcu_ {
	int fd;
};


const wtmcu_led_status_t wtmcu_led_diag_tbl[] = {
	{ "preinit",         NULL,           WTMCU_LED_MODE_BLINK,   WTMCU_LED_COLOR_BLUE    },
	{ "preinit_regular", "led-boot",     WTMCU_LED_MODE_BLINK,   WTMCU_LED_COLOR_BLUE    },
	{ "done",            "led-running",  WTMCU_LED_MODE_ON,      WTMCU_LED_COLOR_BLUE    },
	{ "failsafe",        "led-failsafe", WTMCU_LED_MODE_ON,      WTMCU_LED_COLOR_RED     },
	{ "upgrade",         "led-upgrade",  WTMCU_LED_MODE_BLINK,   WTMCU_LED_COLOR_RED     },
	{ "off",             NULL,           WTMCU_LED_MODE_ON,      WTMCU_LED_COLOR_ORANGE  },
	{ "sleep",           NULL,           WTMCU_LED_MODE_BREATH,  WTMCU_LED_COLOR_BLUE    },
	{ NULL,              NULL,           WTMCU_LED_MODE_INVALID, WTMCU_LED_COLOR_INVALID },
};


static bool
set_interface_attribs(int fd, int speed, int parity, int should_block)
{
	struct termios tty;

	memset(&tty, 0, sizeof(tty));

	if (tcgetattr(fd, &tty) < 0) {
		syslog(LOG_DEBUG, "Error from tcgetattr: %s", strerror(errno));
		return false;
	}

	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);

	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8; // 8-bit chars
	// disable IGNBRK for mismatched speed tests; otherwise receive break
	// as \000 chars
	tty.c_iflag &= ~IGNBRK;                 // ignore break signal
	tty.c_lflag = 0;                        // no signaling chars, no echo,
	                                        // no canonical processing
	tty.c_oflag = 0;                        // no remapping, no delays
	tty.c_cc[VMIN] = should_block ? 1 : 0;
	tty.c_cc[VTIME] = 5;                    // 0.5 seconds read timeout

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off xon/xoff ctrl

	tty.c_cflag |= (CLOCAL | CREAD);        // ignore modem controls,
	                                        // enable reading
	tty.c_cflag &= ~(PARENB | PARODD);      // turn off parity
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	if (tcsetattr(fd, TCSANOW, &tty) != 0) {
		syslog(LOG_DEBUG, "Error from tcsetattr: %s", strerror(errno));
		return false;
	}

	return true;
}

static wtmcu_error_t
communicate(wtmcu_t *hndl, const uint8_t *in, uint8_t *out, size_t out_size)
{
	ssize_t bs;
	short int i;
	uint8_t b;
	char l[32] = { 0 };
	wtmcu_error_t res = WTMCU_ERR_NONE;

	if (hndl == NULL)
		return WTMCU_ERR_BAD;

	int fd = hndl->fd;

	i = 0; bs = 0;
	do {
		bs = write(fd, &in[i], 1);
		snprintf(l + strlen(l), 32 - strlen(l), " %02x", in[i]);
		i++;
		usleep(100);
		if (bs != 1) {
			syslog(LOG_WARNING, "Error writing byte %i: %i, count: %zi", (i - 1), in[i - 1], bs);
			return WTMCU_ERR_IO;
		}
	} while (in[i - 1] != WT_END_BYTE);
	syslog(LOG_DEBUG, "%s%s", "WTMCU REQ:", l);

	if (out != NULL) {
		l[0] = 0;
		i = 0; bs = 0;
		do {
			bs = read(fd, &b, 1);
			snprintf(l + strlen(l), 32 - strlen(l), " %02x", b);
			if (i < out_size) {
				out[i] = b;
			}
			i++;
		} while ((bs == 1) && (b != WT_END_BYTE));
		syslog(LOG_DEBUG, "%s%s", "WTMCU RES:", l);
		if (out[0] != in[0] || out[1] != in[1] || out[2] != out[2]) {
			res = WTMCU_ERR_RESPONSE;
		}
	} else {
		syslog(LOG_DEBUG, "WTMCU RES: ---");
	}

	l[0] = 0;
	i = 0; bs = 0;
	do {
		bs = read(fd, &b, 1);
		snprintf(l + strlen(l), 32 - strlen(l), " %02x", b);
		i++;
	} while ((bs == 1) && (b != WT_END_BYTE));
	syslog(LOG_DEBUG, "%s%s", "WTMCU ACK:", l);

	return res;
}

// ------------------------------------

const char*
wtmcu_strerror(wtmcu_error_t err)
{
	switch (err) {
	case WTMCU_ERR_NONE:
		return "Success";
	case WTMCU_ERR_BAD:
		return "Bad arguments";
	case WTMCU_ERR_IO:
		return "TTY device communication failure";
	case WTMCU_ERR_RESPONSE:
		return "Invalid response received";
	default:
		return "Unknown (not recognized) error";
	}
}

wtmcu_t*
wtmcu_create(const char *device_name)
{
	if (device_name == NULL)
		return NULL;

	wtmcu_t *hndl = malloc(sizeof(wtmcu_t));

	if (hndl == NULL)
		return NULL;

	hndl->fd = open(device_name, O_RDWR | O_NOCTTY | O_SYNC);
	if (hndl->fd < 0) {
		syslog(LOG_ERR, "Error opening MCU %s: %s", device_name, strerror(errno));
		free(hndl);
		return NULL;
	}

	if (!set_interface_attribs(hndl->fd, B19200, 0, 0)) {
		syslog(LOG_ERR, "Error initializing MCU %s: %s", device_name, strerror(errno));
		wtmcu_destroy(hndl);
		return NULL;
	}

	return hndl;
}

void
wtmcu_destroy(wtmcu_t *hndl)
{
	if (hndl != NULL) {
		close(hndl->fd);
		free(hndl);
	}
}

wtmcu_error_t
wtmcu_communicate(wtmcu_t *hndl, const uint8_t *req, size_t req_size, uint8_t *res, size_t res_size)
{
	return communicate(hndl, req, res, res_size);
}


wtmcu_error_t
wtmcu_ver_get(wtmcu_t *hndl, wtmcu_version_t *ver)
{
	if (ver == NULL)
		return WTMCU_ERR_BAD;

	uint8_t req[] = { WT_START_BYTE, 0x03, 0x05, 0, 0, 0, WT_END_BYTE };
	uint8_t res[7] = { 0 };

	wtmcu_error_t err;

	if ((err = communicate(hndl, req, res, 7)) != WTMCU_ERR_NONE)
		return err;

	ver->major = res[4];
	ver->minor = res[5];
	return WTMCU_ERR_NONE;
}

wtmcu_error_t
wtmcu_led_set(wtmcu_t *hndl, wtmcu_led_mode_t mode, wtmcu_led_color_t color)
{
	uint8_t cmd[] = { WT_START_BYTE, 0x26, 0x00, mode | color, 0, 0, WT_END_BYTE };

	return communicate(hndl, cmd, NULL, 0);
}

wtmcu_error_t
wtmcu_fan_set_pwm(wtmcu_t *hndl, uint8_t pwm)
{
	//uint8_t speed = (rate > 100 ? 100 : rate) * 255 / 100;
	uint8_t val = (pwm == WT_END_BYTE ? pwm + 1 : pwm);
	uint8_t req[] = { WT_START_BYTE, 0x02, 0x00, val, 0, 0, WT_END_BYTE };

	return communicate(hndl, req, NULL, 0);
}

wtmcu_error_t
wtmcu_ready_set(wtmcu_t *hndl)
{
	uint8_t req[] = { WT_START_BYTE, 0x03, 0x01, 0, 0, 0, WT_END_BYTE };

	return communicate(hndl, req, NULL, 0);
}

wtmcu_error_t
wtmcu_fan_get_rpm(wtmcu_t *hndl, uint32_t *rpm)
{
	if (rpm == NULL)
		return WTMCU_ERR_BAD;

	uint8_t req[] = { WT_START_BYTE, 0x02, 0x01, 0, 0, 0, WT_END_BYTE };
	uint8_t res[7] = { 0 };

	wtmcu_error_t err;

	if ((err = communicate(hndl, req, res, 7)) != WTMCU_ERR_NONE)
		return err;

	*rpm = res[5] > 0 ? (300000 / res[5]) : 0;
	return WTMCU_ERR_NONE;
}

wtmcu_error_t
wtmcu_temp_get(wtmcu_t *hndl, uint8_t *temp)
{
	if (temp == NULL)
		return WTMCU_ERR_BAD;

	uint8_t req[] = { WT_START_BYTE, 0x03, 0x08, 0, 0, 0, WT_END_BYTE };
	uint8_t res[7] = { 0 };

	wtmcu_error_t err;

	if ((err = communicate(hndl, req, res, 7)) != WTMCU_ERR_NONE)
		return err;

	//*temp = 147.96 - 1.623 * res[5] + 0.0054 * res[5] * res[5];
	//*temp = 97.25 - 0.55 * res[5];
	*temp = (175 - res[5]) * 5 / 9;
	return WTMCU_ERR_NONE;
}

wtmcu_error_t
wtmcu_wol_set(wtmcu_t *hndl, wtmcu_wol_state_t state)
{
	uint8_t cmd[] = { WT_START_BYTE, 0x03, 0x0A, state, 0, 0, WT_END_BYTE };

	return communicate(hndl, cmd, NULL, 0);
}

wtmcu_error_t
wtmcu_wol_get(wtmcu_t *hndl, wtmcu_wol_state_t *state)
{
	if (state == NULL)
		return WTMCU_ERR_BAD;

	uint8_t req[] = { WT_START_BYTE, 0x03, 0x0A, 0x02, 0, 0, WT_END_BYTE };
	uint8_t res[7] = { 0 };

	wtmcu_error_t err;

	if ((err = communicate(hndl, req, res, 7)) != WTMCU_ERR_NONE)
		return err;

	*state = res[5] ? WTMCU_WOL_ON : WTMCU_WOL_OFF;
	return WTMCU_ERR_NONE;
}

wtmcu_error_t
wtmcu_power_set(wtmcu_t *hndl, wtmcu_pwm_mode_t mode, uint8_t delay_seconds)
{
	uint8_t val = (delay_seconds == WT_END_BYTE ? delay_seconds + 1 : delay_seconds);
	uint8_t cmd[] = { WT_START_BYTE, 0x03, 0x03, mode, 0, val, WT_END_BYTE };

	return communicate(hndl, cmd, NULL, 0);
}
