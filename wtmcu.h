#ifndef WTMCU_H_
#define WTMCU_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdint.h>
#include <unistd.h>


#define WT_START_BYTE   0xFA
#define WT_END_BYTE     0xFB

#define WT_FAN_PWM_MIN  0x40
#define WT_FAN_PWM_MAX  0xFF


typedef enum {
	WTMCU_ERR_NONE		=  0,
	WTMCU_ERR_BAD		= -1,
	WTMCU_ERR_IO		= -10,
	WTMCU_ERR_RESPONSE	= -20,
} wtmcu_error_t;

typedef enum {
	WTMCU_OFF	= 0x00,
	WTMCU_ON	= 0x01,
	WTMCU_INVALID	= 0xFF,
} wtmcu_state_t;

typedef enum {
	WTMCU_LED_MODE_OFF	= 0x00,
	WTMCU_LED_MODE_ON	= 0x10,
	WTMCU_LED_MODE_BLINK	= 0x20,
	WTMCU_LED_MODE_BREATH	= 0x40,
	WTMCU_LED_MODE_INVALID	= WTMCU_INVALID,
} wtmcu_led_mode_t;

typedef enum {
	WTMCU_LED_COLOR_BLUE	= 0x01,
	WTMCU_LED_COLOR_ORANGE	= 0x02,
	WTMCU_LED_COLOR_RED	= 0x04,
	WTMCU_LED_COLOR_INVALID = WTMCU_INVALID,
} wtmcu_led_color_t;

typedef enum {
	WTMCU_PWM_SHUTDOWN	= 0x01,
	WTMCU_PWM_SHUTDOWN_UPS	= 0x03,
	WTMCU_PWM_RESET		= 0x02,
	WTMCU_PWM_INVALID	= WTMCU_INVALID,
} wtmcu_pwm_mode_t;

typedef enum {
	WTMCU_WOL_OFF		= WTMCU_OFF,
	WTMCU_WOL_ON		= WTMCU_ON,
	WTMCU_WOL_INVALID	= WTMCU_INVALID,
} wtmcu_wol_state_t;

typedef struct wtmcu_version_ {
	uint8_t major;
	uint8_t minor;
} wtmcu_version_t;

typedef struct wtmcu_led_status_ {
	const char *name;
	const char *led;
	wtmcu_led_mode_t mode;
	wtmcu_led_color_t color;
} wtmcu_led_status_t;

typedef struct wtmcu_ wtmcu_t;


extern const wtmcu_led_status_t wtmcu_led_diag_tbl[];


const char*
wtmcu_strerror(wtmcu_error_t err);

wtmcu_t*
wtmcu_create(const char *device_name);

void
wtmcu_destroy(wtmcu_t *hndl);


wtmcu_error_t
wtmcu_ver_get(wtmcu_t *hndl, wtmcu_version_t *ver);

wtmcu_error_t
wtmcu_led_set(wtmcu_t *hndl, wtmcu_led_mode_t mode, wtmcu_led_color_t color);


wtmcu_error_t
wtmcu_fan_set_pwm(wtmcu_t *hndl, uint8_t pwm);

wtmcu_error_t
wtmcu_fan_get_rpm(wtmcu_t *hndl, uint32_t *rpm);

wtmcu_error_t
wtmcu_wol_set(wtmcu_t *hndl, wtmcu_wol_state_t state);

wtmcu_error_t
wtmcu_wol_get(wtmcu_t *hndl, wtmcu_wol_state_t *state);


wtmcu_error_t
wtmcu_temp_get(wtmcu_t *hndl, uint8_t *temp);

wtmcu_error_t
wtmcu_ready_set(wtmcu_t *hndl);

wtmcu_error_t
wtmcu_communicate(wtmcu_t *hndl, const uint8_t *req, size_t req_size, uint8_t *res, size_t res_size);

wtmcu_error_t
wtmcu_power_set(wtmcu_t *hndl, wtmcu_pwm_mode_t mode, uint8_t delay_seconds);

#endif //WTMCU_H_
