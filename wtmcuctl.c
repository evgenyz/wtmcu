#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

#include "wtmcu.h"


enum {
	OP_NONE,
	OP_GET,
	OP_SET,
	OP_REQ,
	OP_PRE,
};

int main(int argc, char *argv[])
{
	int op = OP_NONE;
	char *attr = NULL;
	char *device_name = NULL;

	opterr = 0;
	int c;

	while ((c = getopt(argc, argv, "Ld:g:s:p:x")) != -1)
		switch (c) {
		case 'L':
			openlog("wtmcuctl", LOG_PERROR | LOG_CONS, LOG_USER);
			break;
		case 'd':
			device_name = optarg;
			break;
		case 'g':
			attr = optarg;
			op = OP_GET;
			break;
		case 's':
			attr = optarg;
			op = OP_SET;
			break;
		case 'x':
			attr = optarg;
			op = OP_REQ;
			break;
		case 'p':
			attr = optarg;
			op = OP_PRE;
			break;
		case '?':
			if (optopt == 'd' || optopt == 'g' || optopt == 's' || optopt == 'p')
				fprintf(stderr, "Option -%c requires an argument.\n", optopt);
			else if (isprint(optopt))
				fprintf(stderr, "Unknown option `-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
			return 1;
		default:
			fprintf(stderr, "Ooops!");
			abort();
		}

	if (device_name == NULL) {
		fprintf(stderr, "WTMCU TTY device name is not set (-d).\n");
		return 1;
	}

	wtmcu_t *wtmcu = wtmcu_create(device_name);

	if (wtmcu == NULL) {
		fprintf(stderr, "Unable to use WTMCU device %s.\n", device_name);
		return 2;
	}

	switch (op) {
	case OP_GET:
		if (!strcmp(attr, "ver")) {
			wtmcu_version_t ver;
			if (wtmcu_ver_get(wtmcu, &ver) == WTMCU_ERR_NONE) {
				printf("MCU Version: %d.%02d.\n", ver.major, ver.minor);
			} else {
				printf("Unable to fetch MCU version information.\n");
			}
		} else if (!strcmp(attr, "temp")) {
			uint8_t temp;
			if (wtmcu_temp_get(wtmcu, &temp) == WTMCU_ERR_NONE) {
				printf("Temperature: %dC.\n", temp);
			} else {
				printf("Unable to fetch temperature information.\n");
			}
		} else if (!strcmp(attr, "wol")) {
			wtmcu_wol_state_t state;
			if (wtmcu_wol_get(wtmcu, &state) == WTMCU_ERR_NONE) {
				printf("WOL state: %s.\n", state == WTMCU_WOL_ON ? "on" : "off");
			} else {
				printf("Unable to fetch WOL state information.\n");
			}
		}
		break;
	case OP_SET:
		if (!strcmp(attr, "led")) {
			if (argc - optind < 2) {
				fprintf(stderr, "Set led command requires two arguments: mode [on, off, blink, breath] and color [blue, orange, red].\n");
			} else {
				wtmcu_led_mode_t mode = WTMCU_LED_MODE_INVALID;
				if (!strcmp("on", argv[optind])) {
					mode = WTMCU_LED_MODE_ON;
				} else if (!strcmp("off", argv[optind])) {
					mode = WTMCU_LED_MODE_OFF;
				} else if (!strcmp("blink", argv[optind])) {
					mode = WTMCU_LED_MODE_BLINK;
				} else if (!strcmp("breath", argv[optind])) {
					mode = WTMCU_LED_MODE_BREATH;
				}
				if (mode == WTMCU_LED_MODE_INVALID) {
					fprintf(stderr, "Set led mode values are: on, off, blink, breath.\n");
				}

				wtmcu_led_color_t color = WTMCU_LED_COLOR_INVALID;
				if (!strcmp("blue", argv[optind + 1])) {
					color = WTMCU_LED_COLOR_BLUE;
				} else if (!strcmp("orange", argv[optind + 1])) {
					color = WTMCU_LED_COLOR_ORANGE;
				} else if (!strcmp("red", argv[optind + 1])) {
					color = WTMCU_LED_COLOR_RED;
				}
				if (color == WTMCU_LED_COLOR_INVALID) {
					fprintf(stderr, "Set led color values are: blue, orange, red.\n");
				}

				if (mode != WTMCU_LED_MODE_INVALID && color != WTMCU_LED_COLOR_INVALID)
					wtmcu_led_set(wtmcu, mode, color);
			}
		} else if (!strcasecmp(attr, "wol")) {
			if (argc - optind < 1) {
				fprintf(stderr, "Set wol command requires an argument: state [on, off].\n");
			} else {
				wtmcu_wol_state_t state = WTMCU_WOL_INVALID;
				if (!strcmp("on", argv[optind])) {
					state = WTMCU_WOL_ON;
				} else if (!strcasecmp("off", argv[optind])) {
					state = WTMCU_WOL_OFF;
				}
				if (state == WTMCU_WOL_INVALID) {
					fprintf(stderr, "Set wol state values are: on, off.\n");
				} else {
					wtmcu_wol_set(wtmcu, state);
				}
			}
		} else if (!strcmp(attr, "power")) {
			if (argc - optind < 1) {
				fprintf(stderr, "Set power command requires an argument: mode [reset, shutdown, shutdown-ups] and optional delay in seconds.\n");
			} else {
				wtmcu_pwm_mode_t mode = WTMCU_PWM_INVALID;
				uint8_t delay = 0;
				if (!strcasecmp("reset", argv[optind])) {
					mode = WTMCU_PWM_RESET;
				} else if (!strcasecmp("shutdown", argv[optind])) {
					mode = WTMCU_PWM_SHUTDOWN;
				} else if (!strcasecmp("shutdown-ups", argv[optind])) {
					mode = WTMCU_PWM_SHUTDOWN_UPS;
				}
				if (argc - optind > 1) {
					delay = strtoul(argv[optind + 1], NULL, 10);
				}
				if (mode == WTMCU_PWM_INVALID) {
					fprintf(stderr, "Set power mode values are: reset, shutdown, shutdown-ups.\n");
				} else {
					wtmcu_power_set(wtmcu, mode, delay);
				}
			}
		}
		break;
	case OP_PRE:
		if (!strcmp(attr, "state")) {
			char l[120] = { 0 };
			for (short int i = 0; wtmcu_led_diag_tbl[i].name != NULL; i++) {
				snprintf(l + strlen(l), 120 - strlen(l), "%s%s", i ? ", " : "", wtmcu_led_diag_tbl[i].name);
			}

			if (argc - optind < 1) {
				fprintf(stderr, "Preset state command requires an argument: state alias [%s].\n", l);
			} else {
				const wtmcu_led_status_t *status = NULL;
				for (short int i = 0; wtmcu_led_diag_tbl[i].name != NULL; i++) {
					if (!strcasecmp(argv[optind], wtmcu_led_diag_tbl[i].name)) {
						status = &wtmcu_led_diag_tbl[i];
						break;
					}
				}

				if (status == NULL) {
					fprintf(stderr, "Preset state values are: %s.\n", l);
				} else {
					wtmcu_led_set(wtmcu, status->mode, status->color);
				}
			}
		} else {
			fprintf(stderr, "Unknown preset: %s.\n", attr);
		}
		break;
	case OP_REQ:
		if (argc - optind < 5) {
			fprintf(stderr, "Custom command requires at least six arguments (size of expected response (or 0) and 5 bytes of the request).\n");
		} else {
			uint8_t req[7] = { WT_START_BYTE, 0, 0, 0, 0, 0, WT_END_BYTE };
			uint8_t res[24] = { 0 };
			req[1] = strtoul(argv[optind + 0], NULL, 0);
			req[2] = strtoul(argv[optind + 1], NULL, 0);
			req[3] = strtoul(argv[optind + 2], NULL, 0);
			req[4] = strtoul(argv[optind + 3], NULL, 0);
			req[5] = strtoul(argv[optind + 4], NULL, 0);

			printf("Request:");
			for (int i = 1; i < 7 - 1; i++) {
				printf(" %02x", req[i]);
			}
			printf(".\n");

			int res_size = strtoul(attr, NULL, 0);
			if (wtmcu_communicate(wtmcu, req, 7, res_size ? res : NULL, res_size > 24 ? 24 : res_size) == WTMCU_ERR_NONE) {
				if (res_size) {
					printf("Response:");
					for (int i = 1; i < res_size - 1; i++) {
						printf(" %02x", res[i]);
					}
					printf(".\n");
				} else {
					printf("Done.\n");
				}
			} else {
				printf("Unable to communicate command.\n");
			}
		}
		break;
	default:
		fprintf(stderr, "Operation (-g, -s, -p or -x) is not defined.\n");
	}
 exit:
	wtmcu_destroy(wtmcu);
	return 0;
}
