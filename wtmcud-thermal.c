#include "wtmcud.h"


#define POW2(x)    ((x) * (x))
#define MINM(a, b) (((a) < (b)) ? (a) : (b))


static struct thermal_state_ {
	uint8_t temp;
	uint8_t fan_pwm;
	uint32_t fan_rpm;
} tstate;


static void
thermal_state_get(void)
{
	wtmcu_temp_get(wtmcu, &tstate.temp);
	wtmcu_fan_get_rpm(wtmcu, &tstate.fan_rpm);
	syslog(LOG_INFO, "Got state: ts.temp=%d, ts.fan_rpm=%d", tstate.temp, tstate.fan_rpm);
}

static void
thermal_state_set_fan(uint8_t fan_pwm)
{
	if (fan_pwm != tstate.fan_pwm) {
		syslog(LOG_NOTICE, "Speed has changed, updating (fan_pwm=%d, ts.fan_pwm=%d)", fan_pwm, tstate.fan_pwm);
		tstate.fan_pwm = fan_pwm;
		wtmcu_fan_set_pwm(wtmcu, tstate.fan_pwm);
	}
}

static void
thermal_control(struct uloop_timeout *to)
{
	syslog(LOG_INFO, "Frame: tmin=%d, Tmax=%d, Thyst=%d, Fmin=%d, Fmax=%d", tframe.temp_min, tframe.temp_max, tframe.temp_hyst, tframe.fan_min_pwm, tframe.fan_max_pwm);

	thermal_state_get();

	uint8_t fan_pwm = tstate.fan_pwm;

	if (tstate.fan_pwm >= tframe.fan_min_pwm && tstate.fan_rpm == 0) {
		syslog(LOG_NOTICE, "Fan stalled (ts.fan_pwm=%d >= Fmin=%d, ts.fan_rpm=%d)", tstate.fan_pwm, tframe.fan_min_pwm, tstate.fan_rpm);
		fan_pwm = MINM(tstate.fan_pwm + 1, tframe.fan_max_pwm);
		tframe.fan_min_pwm = fan_pwm;
		syslog(LOG_NOTICE, "Increasing fan minimal value (Fmin=%d, fan_pwm=%d)", tframe.fan_min_pwm, fan_pwm);
	}

	if (tstate.temp > tframe.temp_min) {
		if (tstate.temp < tframe.temp_max) {
			int l = tframe.fan_min_pwm + (tframe.fan_max_pwm - tframe.fan_min_pwm) * POW2(tstate.temp - tframe.temp_min) / POW2(tframe.temp_max - tframe.temp_min);
			int h = tframe.fan_max_pwm - (tframe.fan_max_pwm - tframe.fan_min_pwm) * POW2(tframe.temp_max - tstate.temp - tframe.temp_hyst) / POW2(tframe.temp_max - tframe.temp_min);
			syslog(LOG_INFO, "Current temperatue is in range (bounds: low=%d, high=%d, fan_pwm=%d)", l, h, fan_pwm);
			if (fan_pwm < l) {
				syslog(LOG_NOTICE, "fan_pwm is now %d (cur<l)", l);
				fan_pwm = l;
			} else if (fan_pwm > h) {
				syslog(LOG_NOTICE, "fan_pwm is now %d (cur>h)", h);
				fan_pwm = h;
			}
		} else {
			syslog(LOG_NOTICE, "Temperature is high, maxing the fan");
			fan_pwm = tframe.fan_max_pwm;
		}
	} else {
		syslog(LOG_INFO, "Temperature is low, the fan is on minimum");
		if (tstate.temp < tframe.temp_min - tframe.temp_hyst) {
			if (tstate.fan_pwm > 0) {
				syslog(LOG_NOTICE, "Temperature is lower than tmin-Thyst, turning off the fan");
				fan_pwm = 0;
			}
		}
	}

	thermal_state_set_fan(fan_pwm);

	uloop_timeout_set(to, tframe.update_timeout * MILLISEC);
}

static struct uloop_timeout thermal_timeout = {
	.cb = thermal_control,
};

// ------------------------------------

void wtmcud_thermal_init(void)
{
	thermal_control(&thermal_timeout);
}

void wtmcud_thermal_finish(void)
{
	uloop_timeout_cancel (&thermal_timeout);
}
