//#include <libubox/blobmsg_json.h>

#include "wtmcud.h"


static struct blob_buf b;

static struct ubus_context *ctx;


enum {
	SET_STATUS_STRING,
	__SET_STATUS_MAX
};

static const struct blobmsg_policy set_state_policy[__SET_STATUS_MAX] = {
	[SET_STATUS_STRING] = { .name = "status", .type = BLOBMSG_TYPE_STRING },
};

static int
ubus_wtmcu_set_state(struct ubus_context      *ctx,
                     struct ubus_object       *obj,
                     struct ubus_request_data *req,
                     const char               *method,
                     struct blob_attr         *msg)
{
	struct blob_attr *tb[__SET_STATUS_MAX];

	blobmsg_parse(set_state_policy, __SET_STATUS_MAX, tb, blob_data(msg), blob_len(msg));
	if (!tb[SET_STATUS_STRING])
		return UBUS_STATUS_INVALID_ARGUMENT;

	char *status_str;

	status_str = blobmsg_get_string(tb[SET_STATUS_STRING]);

	const wtmcu_led_status_t *status = NULL;

	for (short int i = 0; wtmcu_led_diag_tbl[i].name != NULL; i++) {
		if (!strcasecmp(status_str, wtmcu_led_diag_tbl[i].name)) {
			syslog(LOG_NOTICE, "Got status message from ubus (%s)", status_str);
			status = &wtmcu_led_diag_tbl[i];
			break;
		}
	}

	if (status == NULL)
		return UBUS_STATUS_INVALID_ARGUMENT;

	if (wtmcu_led_set(wtmcu, status->mode, status->color) != WTMCU_ERR_NONE)
		return UBUS_STATUS_UNKNOWN_ERROR;

	if (!strcasecmp(status->name, "done")) {
		wtmcud_init();
	}

	//blob_buf_init(&b, 0);
	//blobmsg_add_u32(&b, "rc", strcmp(s1, s2));
	//ubus_send_reply(ctx, req, b.head);
	return UBUS_STATUS_OK;
}

static const struct ubus_method wtmcu_methods[] = {
	UBUS_METHOD("set_state", ubus_wtmcu_set_state, set_state_policy),
};

static struct ubus_object_type wtmcu_object_type =
	UBUS_OBJECT_TYPE("wtmcu", wtmcu_methods);

static struct ubus_object wtmcu_ubus_object = {
	.name = "wtmcu",
	.type = &wtmcu_object_type,
	.methods = wtmcu_methods,
	.n_methods = ARRAY_SIZE(wtmcu_methods),
};

// ------------------------------------

void wtmcud_ubus_init(void)
{
	ctx = ubus_connect(NULL);
	if (ctx) {
		ubus_add_uloop(ctx);
		int ret = ubus_add_object(ctx, &wtmcu_ubus_object);
		if (ret) {
			syslog(LOG_WARNING, "Failed to add ubus object: %s", ubus_strerror(ret));
		}
	} else {
		syslog(LOG_WARNING, "Failed to connect to ubus");
	}
}

void wtmcud_ubus_finish(void)
{
	ubus_free(ctx);
}
