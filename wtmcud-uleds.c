#include <fcntl.h>
#include <limits.h>
#include <linux/uleds.h>

#include "wtmcud.h"


#define DT_PREFIX "/proc/device-tree"


struct led_fd {
	struct uloop_fd ufd;
	const char *label;
	const char *alias;
	const wtmcu_led_status_t *status;
};


static struct led_fd leds[] = {
	{.alias = "led-boot"},
	{.alias = "led-running"},
	{.alias = "led-failsafe"},
	{.alias = "led-upgrade"},
	{.alias = NULL},
};


static void
uleds_led_fs_cb(struct uloop_fd *fd, unsigned int events)
{
	for (short int i = 0; leds[i].alias != NULL; i++) {
		if (leds[i].ufd.fd == fd->fd) {
			int brightness;
			int bytes = read(fd->fd, &brightness, sizeof(brightness));
			syslog(LOG_NOTICE, "Got event from uleds %s (brightness: %d)", leds[i].alias, brightness);
			if (bytes > 0 && brightness) {
				if (wtmcu_led_set(wtmcu, leds[i].status->mode, leds[i].status->color) != WTMCU_ERR_NONE) {
					syslog(LOG_WARNING, "Unable to set uleds %s", leds[i].alias);
				}
				if (!strcasecmp(leds[i].status->name, "done")) {
					wtmcud_init();
				}
			}
		}
	}
}

static bool
uleds_label_get(const char *alias, struct uleds_user_dev *uleds_dev)
{
	int fd, bytes;

	char entry[PATH_MAX];
	snprintf(entry, PATH_MAX, DT_PREFIX"/aliases/%s", alias);
	fd = open(entry, O_RDONLY);
	if (fd < 0) {
		syslog(LOG_NOTICE, "Error opening alias %s: %s", entry, strerror(errno));
		return false;
	}
	bytes = read(fd, entry, PATH_MAX);
	entry[bytes] = '\0';
	close(fd);

	if (bytes > 0) {
		char label[PATH_MAX];
		snprintf(label, PATH_MAX, DT_PREFIX"%s/label", entry);
		fd = open(label, O_RDONLY);
		if (fd < 0) {
			syslog(LOG_WARNING, "Error opening label %s: %s", label, strerror(errno));
			return false;
		}
		uleds_dev->name[0] = '\0';
		bytes = read(fd, uleds_dev->name, LED_MAX_NAME_SIZE);
		close(fd);
	}

	return bytes > 0;
}

static bool
uleds_create_dev(struct uloop_fd *ufd, struct uleds_user_dev *uleds_dev)
{
	ufd->fd = open("/dev/uleds", O_RDWR | O_NONBLOCK);
	if (ufd->fd < 0) {
		syslog(LOG_WARNING, "Error opening /dev/uleds: %s", strerror(errno));
		return false;
	}

	int ret = write(ufd->fd, uleds_dev, sizeof(*uleds_dev));
	if (ret == -1) {
		syslog(LOG_WARNING, "Error writing to /dev/uleds: %s", strerror(errno));
		close(ufd->fd);
		ufd->fd = 0;
		return false;
	}

	return true;
}

// ------------------------------------

void wtmcud_uleds_init(void)
{
	struct uleds_user_dev uleds_dev = {.max_brightness = 1};

	for (short int i = 0; leds[i].alias != NULL; i++) {
		for (short int di = 0; wtmcu_led_diag_tbl[di].name != NULL; di++) {
			if (wtmcu_led_diag_tbl[di].led == NULL)
				continue;
			if (!strcasecmp(leds[i].alias, wtmcu_led_diag_tbl[di].led)) {
				leds[i].status = &wtmcu_led_diag_tbl[di];
				if (uleds_label_get(leds[i].alias, &uleds_dev)) {
					if (uleds_create_dev(&leds[i].ufd, &uleds_dev)) {
						leds[i].ufd.cb = uleds_led_fs_cb;
						uloop_fd_add(&leds[i].ufd, ULOOP_READ);
					}
				}
				break;
			}
		}
	}
}

void wtmcud_uleds_finish(void)
{
	for (short int i = 0; leds[i].alias != NULL; i++) {
		if (leds[i].ufd.fd > 0) {
			uloop_fd_delete(&leds[i].ufd);
			close(leds[i].ufd.fd);
		}
	}
}
