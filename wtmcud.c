#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#include <net/if.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>
#include <linux/netlink.h>

#include "wtmcud.h"


static int
char_to_log_level(const char clevel)
{
	switch (clevel) {
	case 'E':
	case 'e':
		return LOG_ERR;
	case 'W':
	case 'w':
		return LOG_WARNING;
	case 'I':
	case 'i':
		return LOG_INFO;
	case 'N':
	case 'n':
		return LOG_NOTICE;
	case 'D':
	case 'd':
	default:
		return LOG_DEBUG;
	}
}


wtmcu_t *wtmcu = NULL;

thermal_frame_t tframe = {
	.temp_min = 55,
	.temp_max = 75,
	.temp_hyst = 5,
	.fan_min_pwm = WT_FAN_PWM_MIN,
	.fan_max_pwm = WT_FAN_PWM_MAX,
	.update_timeout = 20,
};

const char *wol_iface = NULL;


static void
wtmcud_wol_set(const char *wol_iface)
{
	syslog(LOG_INFO, "WOL: %s", wol_iface != NULL ? "On" : "Off");
	wtmcu_wol_set(wtmcu, wol_iface != NULL ? WTMCU_WOL_ON : WTMCU_WOL_OFF);
	if (wol_iface != NULL) {
		struct ethtool_wolinfo woli;
		woli.wolopts = WAKE_MAGIC;
		woli.cmd = ETHTOOL_SWOL;
		int fd = socket(AF_INET, SOCK_DGRAM, 0);
		if (fd < 0)
			fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_GENERIC);
		if (fd < 0) {
			syslog(LOG_WARNING, "Unable to set WOL status: %s (socket)", strerror(errno));
		} else {
			struct ifreq ifr = { 0 };
			ifr.ifr_data = (void *)&woli;
			strncpy(ifr.ifr_name, wol_iface, IFNAMSIZ - 1);
			if (ioctl(fd, SIOCETHTOOL, &ifr))
				syslog(LOG_WARNING, "Unable to set WOL status: %s (ioctl)", strerror(errno));
		}
	}
}

void
wtmcud_init(void)
{
	syslog(LOG_INFO, "MCU ready (WOL and LED)");
	wtmcud_wol_set(wol_iface);
	wtmcu_ready_set(wtmcu);
}

int
main(int argc, char **argv)
{
	int ret = 0;

	const char *wtmcu_device = NULL;
	int log_level = LOG_NOTICE;
	bool log_cons = false;

	int c;

	while ((c = getopt(argc, argv, "d:T:t:h:u:w:l:L:")) != -1) {
		switch (c) {
		case 'd':
			wtmcu_device = optarg;
			break;
		case 'T':
			tframe.temp_max = strtoul(optarg, NULL, 0);
			break;
		case 't':
			tframe.temp_min = strtoul(optarg, NULL, 0);
			break;
		case 'h':
			tframe.temp_hyst = strtoul(optarg, NULL, 0);
			break;
		case 'u':
			tframe.update_timeout = strtoul(optarg, NULL, 0);
			break;
		case 'L':
			log_cons = true;
		case 'l':
			log_level = char_to_log_level(optarg ? optarg[0] : 'D');
			break;
		case 'w':
			wol_iface = optarg;
			break;
		default:
			abort();
		}
	}

	if (tframe.temp_max <= tframe.temp_min) {
		syslog(LOG_ERR, "Invalid temperature range specified: Tmax (%d) <= tmin (%d)\n", tframe.temp_max, tframe.temp_min);
		return 1;
	}

	if (tframe.update_timeout < 1 || tframe.update_timeout > 600) {
		syslog(LOG_ERR, "Invalid timeout specified: %d (acceptable values: 1 - 600 seconds)\n", tframe.update_timeout);
		return 1;
	}

	signal(SIGPIPE, SIG_IGN);
	uloop_init();

	openlog("wtmcud", (log_cons ? LOG_PERROR : 0) | LOG_PID | LOG_CONS, LOG_DAEMON);
	setlogmask(LOG_UPTO(log_level));

	syslog(LOG_NOTICE, "Thermal frame: tmin=%d, Tmax=%d, Thyst=%d, Fmin=%d, Fmax=%d", tframe.temp_min, tframe.temp_max, tframe.temp_hyst, tframe.fan_min_pwm, tframe.fan_max_pwm);
	syslog(LOG_NOTICE, "WOL interface: %s", wol_iface);
	syslog(LOG_NOTICE, "WTMCU device: %s", wtmcu_device);
	syslog(LOG_NOTICE, "-----------------------------------------------------------");

	wtmcu = wtmcu_create(wtmcu_device);
	if (wtmcu == NULL) {
		syslog(LOG_ERR, "Unable to initialize WTMCU device %s", wtmcu_device);
		ret = 2;
		goto exit;
	}

	wtmcu_version_t ver;
	wtmcu_error_t err;
	err = wtmcu_ver_get(wtmcu, &ver);
	if (err != WTMCU_ERR_NONE) {
		syslog(LOG_ERR, "Error communicating with WTMCU device, unable to obtain version information: %s", wtmcu_strerror(err));
		ret = 2;
		goto exit;
	} else {
		syslog(LOG_NOTICE, "Got WTMCU v%d.%02d", ver.major, ver.minor);
	}

	usleep(1 * MICROSEC);

	syslog(LOG_INFO, "Stopping the fan on startup");
	wtmcu_fan_set_pwm(wtmcu, 0);

	wtmcud_thermal_init();
	wtmcud_uleds_init();
	wtmcud_ubus_init();

	uloop_run();

	syslog(LOG_INFO, "Stopping the fan on exit");
	wtmcu_fan_set_pwm(wtmcu, 0);

exit:
	wtmcud_ubus_finish();
	wtmcud_uleds_finish();
	wtmcud_thermal_finish();
	uloop_done();
	wtmcu_destroy(wtmcu);
	closelog();
	return ret;
}
