#ifndef WTMCUD_H_
#define WTMCUD_H_

#include <stdint.h>
#include <syslog.h>

#include "libubus.h"
#include "wtmcu.h"


#define MILLISEC   1000
#define MICROSEC   1000 * MILLISEC


typedef struct thermal_frame_ {
	uint8_t temp_max;
	uint8_t temp_min;
	uint8_t temp_hyst;
	uint8_t fan_min_pwm;
	uint8_t fan_max_pwm;
	unsigned int update_timeout;
} thermal_frame_t;

extern thermal_frame_t tframe;


extern wtmcu_t *wtmcu;


void wtmcud_init(void);

void wtmcud_thermal_init(void);
void wtmcud_thermal_finish(void);

void wtmcud_uleds_init(void);
void wtmcud_uleds_finish(void);

void wtmcud_ubus_init(void);
void wtmcud_ubus_finish(void);

#endif //WTMCUD_H_

